function Prayer(latitude, longitude){
    fetch('http://api.aladhan.com/v1/calendar?latitude='+latitude+'&longitude='+longitude+'&method=2')
    .then(Response => Response.json())
    .then (function(Response){
        let date = new Date();
        let today = date.getDate();

        let data = Response.data[0].timings;

        let app = document.getElementById('app');
        let table = document.createElement('table');
        let tbody = document.createElement('tbody');

        for(i in data){
            let row = tbody.insertRow();
            let name = row.insertCell(0);
            let time = row.insertCell(1);
            name.innerHTML = i;
            time.innerHTML = data[i];
            tbody.appendChild(row);
        }
        table.appendChild(tbody);
        app.appendChild(table);
    });
}

function success(position){
    Prayer(position.coords.latitude, position.coords.longitude);
}

function error(){
    Prayer('-6.200000', '106.816666');
}

function userLocation(){
    if(!navigator.geolocation){
        alert('Geolocation tidak mendukung server anda, silahkan gunakan broser lain')
    } else{
        navigator.geolocation.getCurrentPosition(success, error);
    }
}

function index(){
    let app = document.getElementById('app');
    let h3  = document.createElement('h3');
    h3.innerHTML = 'Prayer-Times';

    app.appendChild(h3);

    userLocation();
}

index();